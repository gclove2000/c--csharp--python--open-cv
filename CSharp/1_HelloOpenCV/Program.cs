﻿using OpenCvSharp;

namespace _1_HelloOpenCV
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mat image = new Mat(300,600,MatType.CV_8UC3);

            image.Circle(new Point(300, 200), 100, new Scalar(25, 110, 288), -100);
            image.Circle(new Point(400, 200), 100, new Scalar(255, 123, 127), -100);

            Cv2.ImShow("Show Window",image);
            Cv2.WaitKey(0);
            //Console.WriteLine("Hello, World!");

            Console.ReadKey();
        }
    }
}
